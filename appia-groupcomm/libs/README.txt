This directory contains all external tools used by Appia.
   
  - File(s): jgcs-0.6.1.jar
    Project name: jGCS
    Version: 0.6.1
    License: Modified BSD (http://jgcs.sourceforge.net/license.html)
    Web site: http://jgcs.sourceforge.net
    Description: 
    	The jGCS library provides a generic interface for Group Communication. 
    	This interface can be used by applications that need primitives from 
    	simple IP Multicast group communication to virtual synchrony or atomic 
    	broadcast. Its a common interface to several existing toolkits that 
    	provide different APIs.
    Notes:
   
  - File(s): flanagan.jar
    Project name: Michael Thomas Flanagan's Java Scientific Library 
    Version: 
    License: (http://www.ee.ucl.ac.uk/~mflanaga/java/)
    Web site: http://www.ee.ucl.ac.uk/~mflanaga/java/
    Description: 
    	Scientific Library with several useful Math functions.
    Notes:
    	Only used by the Phi failure detector at this moment.
   
