package pt.tecnico.kn.rsts;

import org.junit.Assert;
import org.junit.Test;
import pt.tecnico.kn.rsts.data_type.Tuple;

/**
 * Test our tuple match
 */
public class TupleMatchTest {

    @Test
    public void testTupleMatch() {
        Tuple t = new Tuple("s1", "s2", "s3");

        Assert.assertTrue(t.match(new Tuple("s1", "s2", "s3")));

        Assert.assertTrue(t.match(new Tuple("*", "s2", "s3")));
        Assert.assertTrue(t.match(new Tuple("s1", "*", "s3")));
        Assert.assertTrue(t.match(new Tuple("s1", "s2", "*")));

        Assert.assertTrue(t.match(new Tuple("*", "*", "s3")));
        Assert.assertTrue(t.match(new Tuple("s1", "*", "*")));
        Assert.assertTrue(t.match(new Tuple("*", "s2", "*")));

        Assert.assertTrue(t.match(new Tuple("*", "*", "*")));

        Assert.assertFalse(t.match(new Tuple("s4", "s2", "s3")));
        Assert.assertFalse(t.match(new Tuple("s4", "*", "s3")));
        Assert.assertFalse(t.match(new Tuple("s4", "s2", "*")));
        Assert.assertFalse(t.match(new Tuple("s4", "*", "*")));

        Assert.assertFalse(t.match(new Tuple("s1", "s4", "s3")));
        Assert.assertFalse(t.match(new Tuple("*", "s4", "s3")));
        Assert.assertFalse(t.match(new Tuple("s1", "s4", "*")));
        Assert.assertFalse(t.match(new Tuple("*", "s4", "*")));

        Assert.assertFalse(t.match(new Tuple("s1", "s2", "s4")));
        Assert.assertFalse(t.match(new Tuple("*", "s2", "s4")));
        Assert.assertFalse(t.match(new Tuple("s1", "*", "s4")));
        Assert.assertFalse(t.match(new Tuple("*", "*", "s4")));
    }
}
