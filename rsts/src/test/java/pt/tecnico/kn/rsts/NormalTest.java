package pt.tecnico.kn.rsts;

import org.hamcrest.CoreMatchers;
import org.junit.*;
import net.sf.jgcs.JGCSException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pt.tecnico.kn.rsts.data_type.Tuple;
import pt.tecnico.kn.rsts.network.RstsServer;
import pt.tecnico.kn.rsts.storage.MemoryStorage;

import java.io.IOException;
import java.util.concurrent.*;

/**
 * Test for our implementation of Rsts server
 */
public class NormalTest {
    private static final Logger Log = LoggerFactory.getLogger(NormalTest.class);
    private static RstsServer[] servers;
    private static ExecutorService executorService;
    private static int serverCount = 5;

    @BeforeClass
    public static void setUpClass() throws InterruptedException, JGCSException {
        executorService = Executors.newCachedThreadPool();

        servers = new RstsServer[serverCount];
        String groupName = "test_normal";
        for (int i = 0; i < serverCount; i++) {
            servers[i] = new RstsServer(groupName, "conf/server.xml", new MemoryStorage());
            servers[i].start(serverCount);
        }

        Thread.sleep(2000);
    }

    @Before
    public void setupTest() throws JGCSException, InterruptedException {
        for (int i = 0; i < serverCount; i++) {
            servers[i].dropData();
        }
    }

    @Test
    public void testBasicReadWrite() throws IOException, InterruptedException, ExecutionException {
        Tuple input = new Tuple("s1", "s2", "s3");
        // Reading from the tuple space. This should block since we have nothing
        Future<Tuple> ret = executorService.submit(() -> {
            Tuple pattern = new Tuple("s1", "s2", "s3");
            Log.info("Reading tuple matching: {}", pattern);
            Tuple out = null;
            try {
                out = servers[0].read(pattern);
            } catch (Exception e) {
                Log.warn("Unable to read tuple", e);
            }
            return out;
        });

        // Writing to the tuple space, this should cause the reader to unblock and return result
        executorService.submit(() -> {
            Log.info("Writing tuple: {}", input);
            try {
                servers[1].write(input);
            } catch (Exception e) {
                Log.warn("Unable to write tuple", e);
            }
        });

        Tuple output = ret.get();
        Assert.assertEquals(input, output);
    }

    @Test
    public void testBasicReadWritePattern() throws IOException, InterruptedException, ExecutionException {
        Tuple input = new Tuple("s1", "s2", "s3");
        // Reading from the tuple space. This should block since we have nothing
        Future<Tuple> ret = executorService.submit(() -> {
            Tuple pattern = new Tuple("s1", "*", "s3");
            Log.info("Reading tuple matching: {}", pattern);
            Tuple out = null;
            try {
                out = servers[3].read(pattern);
            } catch (Exception e) {
                Log.warn("Unable to read tuple", e);
            }
            return out;
        });

        // Writing to the tuple space, this should cause the reader to unblock and return result
        executorService.submit(() -> {
            Log.info("Writing tuple: {}", input);
            try {
                servers[2].write(input);
            } catch (Exception e) {
                Log.warn("Unable to write tuple", e);
            }
        });

        Tuple output = ret.get();
        Assert.assertEquals(input, output);
    }

    @Test
    public void testConcurrentTake() throws IOException, InterruptedException, ExecutionException {
        for(int i = 0; i < 10; i++) {
            Tuple input1 = new Tuple("s1", "s2", "s3");
            Tuple input2 = new Tuple("s1", "s4", "s3");

            // Reading from the tuple space. This should block since we have nothing
            Future<Tuple> ret1 = executorService.submit(() -> {
                Tuple pattern = new Tuple("s1", "*", "s3");
                Log.info("Reading tuple matching: {}", pattern);
                Tuple out = null;
                try {
                    out = servers[3].take(pattern);
                } catch (Exception e) {
                    Log.warn("Unable to read tuple", e);
                }
                return out;
            });
            Future<Tuple> ret2 = executorService.submit(() -> {
                Tuple pattern = new Tuple("s1", "*", "s3");
                Log.info("Reading tuple matching: {}", pattern);
                Tuple out = null;
                try {
                    out = servers[1].take(pattern);
                } catch (Exception e) {
                    Log.warn("Unable to read tuple", e);
                }
                return out;
            });

            // Writing to the tuple space, this should cause the reader to unblock and return result
            executorService.submit(() -> {
                Log.info("Writing tuple: {}", input1);
                try {
                    servers[2].write(input1);
                } catch (Exception e) {
                    Log.warn("Unable to write tuple", e);
                }
            });
            executorService.submit(() -> {
                Log.info("Writing tuple: {}", input2);
                try {
                    servers[2].write(input2);
                } catch (Exception e) {
                    Log.warn("Unable to write tuple", e);
                }
            });

            Tuple output1 = ret1.get();
            Tuple output2 = ret2.get();

            Assert.assertThat(
                    new Tuple[]{output1, output2},
                    CoreMatchers.either(CoreMatchers.is(new Tuple[]{input1, input2}))
                            .or(CoreMatchers.is(new Tuple[]{input2, input1})));
        }
    }
}
