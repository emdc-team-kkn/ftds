package pt.tecnico.kn.rsts;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pt.tecnico.kn.rsts.data_type.Tuple;
import pt.tecnico.kn.rsts.network.RstsServer;
import pt.tecnico.kn.rsts.network.ServerPauseException;
import pt.tecnico.kn.rsts.storage.MemoryStorage;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Test for minority view handling
 */
public class MinorityViewTest {

    private static final Logger Log = LoggerFactory.getLogger(NormalTest.class);
    private static RstsServer[] servers;
    private static ExecutorService executorService;
    private static int serverCount = 5;

    @BeforeClass
    public static void setUpClass() throws InterruptedException, IOException {
        executorService = Executors.newCachedThreadPool();
    }

    @Test(expected = ServerPauseException.class)
    public void testWrite() throws Throwable {
        RstsServer[] servers = new RstsServer[serverCount];
        for (int i = 0; i < serverCount / 2; i++) {
            servers[i] = new RstsServer("test_minority_write", "conf/server.xml", new MemoryStorage());
            servers[i].start(serverCount);
        }

        Future<Tuple> ret1 = executorService.submit(() -> {
            Tuple in = new Tuple("s1", "s2", "s3");
            Log.info("Reading tuple matching: {}", in);
            servers[0].write(in);
            return null;
        });

        try {
            ret1.get();
        } catch (ExecutionException e) {
            throw e.getCause();
        }
    }

    @Test(expected = ServerPauseException.class)
    public void testTake() throws Throwable {
        RstsServer[] servers = new RstsServer[serverCount];
        for (int i = 0; i < serverCount / 2; i++) {
            servers[i] = new RstsServer("test_minority_take", "conf/server.xml", new MemoryStorage());
            servers[i].start(serverCount);
        }

        Future<Tuple> ret1 = executorService.submit(() -> {
            Tuple pattern = new Tuple("s1", "s2", "s3");
            Log.info("Reading tuple matching: {}", pattern);
            return servers[0].take(pattern);
        });

        try {
            ret1.get();
        } catch (ExecutionException e) {
            throw e.getCause();
        }
    }
}
