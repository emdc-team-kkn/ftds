package pt.tecnico.kn.rsts;

import net.sf.jgcs.JGCSException;
import org.junit.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pt.tecnico.kn.rsts.data_type.Tuple;
import pt.tecnico.kn.rsts.network.RstsServer;
import pt.tecnico.kn.rsts.storage.MemoryStorage;
import pt.tecnico.kn.rsts.utils.TuplesGen;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.LongAdder;

/**
 * Stress test our system
 */
public class StressTest {
    private static final Logger Log = LoggerFactory.getLogger(NormalTest.class);
    private static RstsServer[] servers;
    private static ExecutorService writeExecutorService;
    private static ExecutorService readExecutorService;
    private static ExecutorService takeExecutorService;
    private static int serverCount = 5;
    private static List<Tuple> tupleList = new ArrayList<>();
    private static final Tuple SENTINEL = new Tuple("SENTINEL", "SENTINEL", "SENTINEL");

    @BeforeClass
    public static void setUpClass() throws InterruptedException, JGCSException {
        writeExecutorService = Executors.newFixedThreadPool(serverCount);
        readExecutorService = Executors.newFixedThreadPool(serverCount);
        takeExecutorService = Executors.newFixedThreadPool(serverCount);

        servers = new RstsServer[serverCount];
        String groupName = "test_stress";
        for (int i = 0; i < serverCount; i++) {
            servers[i] = new RstsServer(groupName, "conf/server.xml", new MemoryStorage());
            servers[i].start(serverCount);
        }

        Thread.sleep(2000);

        TuplesGen.genSubset(3, 0, new HashSet<>(), TuplesGen.sources, tupleList);
    }

    @AfterClass
    public static void teardownClass() {
        writeExecutorService.shutdownNow();
        readExecutorService.shutdownNow();
        takeExecutorService.shutdownNow();
    }

    @Before
    public void setupTest() throws JGCSException, InterruptedException {
        for (int i = 0; i < serverCount; i++) {
            servers[i].dropData();
        }
    }

    @Test
    public void testAll() throws IOException, InterruptedException, ExecutionException {
        int operationCount = 2000;

        Map<Tuple, LongAdder> takeCount = new ConcurrentHashMap<>();
        List<Future<Tuple[]>> results = new ArrayList<>();

        // Write in
        for(Tuple t: tupleList) {
            for (RstsServer server : servers) {
                writeExecutorService.submit(() -> {
                    try {
                        server.write(t);
                    } catch (Exception e) {
                        Log.debug("Unable to write tuple");
                    }
                });
            }
        }

        for (RstsServer server : servers) {
            writeExecutorService.submit(() -> {
                // Write the SENTINEL tuple in
                Log.debug("Writing SENTINEL");
                try {
                    server.write(SENTINEL);
                } catch (Exception e) {
                    Log.debug("Unable to write tuple");
                }
                Log.debug("Finish writing tuples");
            });
        }

        // Take
        Random rd = new Random();
        int counter = 0;
        while(++counter <= operationCount / 2) {
            results.add(takeExecutorService.submit(() -> {
                RstsServer server = servers[rd.nextInt(serverCount)];
                Tuple pattern;
                do {
                    pattern = TuplesGen.getRandomTuple(TuplesGen.sources);
                } while (takeCount.containsKey(pattern) && takeCount.get(pattern).intValue() >= serverCount - 1);

                if (!takeCount.containsKey(pattern)) {
                    takeCount.put(pattern, new LongAdder());
                }
                takeCount.get(pattern).increment();
                return new Tuple[]{pattern, server.take(pattern)};
            }));
        }

        while(++counter < operationCount) {
            results.add(readExecutorService.submit(() -> {
                RstsServer server = servers[rd.nextInt(serverCount)];
                Tuple pattern = TuplesGen.getRandomTuple(TuplesGen.sources);
                return new Tuple[]{pattern, server.read(pattern)};
            }));
        }

        // Taking the SENTINEL tuple out, because SENTINEL is put in last,
        // it's highly likely that all other tuples has finished writing.
        for (RstsServer server : servers) {
            results.add(takeExecutorService.submit(() -> new Tuple[]{SENTINEL, server.take(SENTINEL)}));
        }

        for(Future<Tuple[]> ret: results) {
            Tuple[] out = ret.get();
            Assert.assertTrue(out[1].match(out[0]));
        }
    }
}
