package pt.tecnico.kn.rsts.runner;

import asg.cliche.ShellFactory;
import net.sf.jgcs.*;
import org.kohsuke.args4j.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pt.tecnico.kn.rsts.benchmark.BaseBenchmark;
import pt.tecnico.kn.rsts.benchmark.ReadWriteBenchmark;
import pt.tecnico.kn.rsts.benchmark.ReadWriteTakeBenchmark;
import pt.tecnico.kn.rsts.benchmark.WriteBenchmark;
import pt.tecnico.kn.rsts.network.*;
import pt.tecnico.kn.rsts.data_type.Tuple;
import pt.tecnico.kn.rsts.storage.IStorage;
import pt.tecnico.kn.rsts.storage.MemoryStorage;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Main class of the program
 */
public class Program {
    private static final Logger Log = LoggerFactory.getLogger(Program.class);
    private static RstsServer server;

    private static class MyOption {

        @Option(name = "-c", usage = "Configuration file for APPIA group.",
                required = true, metaVar = "config-file")
        public String serverConfig;

        @Option(name = "-n", usage = "APPIA group name.", metaVar = "group-name")
        public String name = "rsts";

        @Option(name = "-s", usage = "The size of the cluster.", metaVar = "cluster-size")
        public int clusterSize = 5;

        @Option(name = "-b", usage = "Run benchmark")
        public int benchmark = -1;
    }

    public static void main(String[] args) {
        int ret = 0;
        Log.info("RSTS is starting");

        MyOption options = new MyOption();
        CmdLineParser parser = new CmdLineParser(options);
        try {
            parser.parseArgument(args);
            IStorage storage = new MemoryStorage();
            if (options.benchmark == -1) {
                server = new RstsServer(options.name, options.serverConfig, storage);
                server.start(options.clusterSize);
                CommandLine cmd = new CommandLine(server);
                ShellFactory.createConsoleShell("rsts", "", cmd).commandLoop(); // and three.
            } else {
                BaseBenchmark benchmark = new ReadWriteTakeBenchmark(options.clusterSize, options.clusterSize, 2000, options.serverConfig);
                benchmark.execute();
            }
        } catch (CmdLineException e) {
            System.out.println(e.getLocalizedMessage());
            parser.printUsage(System.out);
            System.out.println("Example: ");
            System.out.printf("java %s%s%n",
                    Program.class.getCanonicalName(),
                    parser.printExample(OptionHandlerFilter.ALL)
            );
            ret = -1;
        }  catch (JGCSException e) {
            Log.warn("Exception when creating protocol", e);
            ret = -2;
        } catch (IOException e) {
            Log.warn("IO error", e);
            ret = -3;
        } catch (InterruptedException e) {
            Log.warn("Benchmark exception", e);
            ret = -4;
        }

        Log.info("RSTS is shutting down");
        System.exit(ret);
    }
}
