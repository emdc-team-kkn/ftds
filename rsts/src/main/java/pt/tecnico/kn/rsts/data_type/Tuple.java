package pt.tecnico.kn.rsts.data_type;

import java.io.Serializable;
import java.util.Objects;

/**
 * This class represent a tuple in our tuple space
 */
public class Tuple implements Serializable {

    private static final long serialVersionUID = 2405540754619512544L;

    private final String s1;
    private final String s2;
    private final String s3;

    public Tuple(String s1, String s2, String s3) {
        this.s1 = s1;
        this.s2 = s2;
        this.s3 = s3;
    }

    /**
     * Check if a tuple match the supplied pattern.
     * If an element in the pattern is *, that element will match everything
     * @param pattern The pattern
     * @return true if matched, false if not
     */
    public boolean match(Tuple pattern) {
        if (this == pattern) {
            return true;
        }
        if (pattern == null) {
            return false;
        }
        if (pattern.s1.equals("*")) {
            if (pattern.s2.equals("*")) {
                return pattern.s3.equals("*") || s3.equals(pattern.s3);
            }
            // s2 is not equal to "*"
            if (pattern.s3.equals("*")) {
                return pattern.s2.equals("*") || s2.equals(pattern.s2);
            }
            // s2, s3 is not equal to "*"
            return s2.equals(pattern.s2) && s3.equals(pattern.s3);
        }
        // s1 is not equal to "*"
        if (pattern.s2.equals("*")) {
            return (pattern.s3.equals("*") || s3.equals(pattern.s3)) && s1.equals(pattern.s1);
        }
        // s1, s2 is not equal to "*"
        if (pattern.s3.equals("*")) {
            return s1.equals(pattern.s1) && s2.equals(pattern.s2);
        }

        // s1, s2, s3 is not equal to "*"
        return s1.equals(pattern.s1) && s2.equals(pattern.s2) && s3.equals(pattern.s3);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tuple tuple = (Tuple) o;
        return Objects.equals(s1, tuple.s1) &&
                Objects.equals(s2, tuple.s2) &&
                Objects.equals(s3, tuple.s3);
    }

    @Override
    public int hashCode() {
        return Objects.hash(s1, s2, s3);
    }

    @Override
    public String toString() {
        return "Tuple {" +
                "'" + s1 + '\'' +
                ", '" + s2 + '\'' +
                ", '" + s3 + '\'' +
                '}';
    }
}
