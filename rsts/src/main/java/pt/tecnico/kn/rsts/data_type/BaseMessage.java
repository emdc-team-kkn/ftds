package pt.tecnico.kn.rsts.data_type;

import java.io.*;

/**
 * Base class for our message
 */
public abstract class BaseMessage {
    protected byte[] buffer;
    protected MessageType command;
    protected boolean marshaled;
    protected String cookies = "DEFAULT_COOKIES";

    public BaseMessage() {

    }

    public BaseMessage(byte[] buffer) {
        this.buffer = buffer;
        this.marshaled = true;
    }

    public void marshal() throws IOException {
        if (marshaled) {
            return;
        }
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream os = new ObjectOutputStream(bos);
        os.writeObject(command);
        os.writeUTF(cookies);

        marshalPrivate(os);

        os.close();
        bos.close();
        buffer = bos.toByteArray();
        marshaled = true;
    }

    public void unmarshal() throws IOException, ClassNotFoundException {
        if (!marshaled) {
            return;
        }
        ByteArrayInputStream bis = new ByteArrayInputStream(buffer);
        ObjectInputStream is = new ObjectInputStream(bis);
        command = (MessageType) is.readObject();
        cookies = is.readUTF();

        unmarshalPrivate(is);

        is.close();
        bis.close();
        marshaled = false;
    }

    protected abstract void marshalPrivate(ObjectOutputStream os) throws IOException;
    protected abstract void unmarshalPrivate(ObjectInputStream is) throws IOException, ClassNotFoundException;

    public byte[] getPayload() throws IOException {
        if (!marshaled) {
            marshal();
        }
        return buffer;
    }

    public MessageType getCommand() {
        return command;
    }

    public void setCommand(MessageType command) {
        this.command = command;
    }

    public String getCookies() {
        return cookies;
    }

    public void setCookies(String cookies) {
        this.cookies = cookies;
    }
}
