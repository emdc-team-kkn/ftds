package pt.tecnico.kn.rsts.data_type;

import pt.tecnico.kn.rsts.storage.Key;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * This message is used to abort the take operation.
 */
public class UnlockRequest extends BaseMessage {

    public UnlockRequest() {
        this.marshaled = false;
    }

    public UnlockRequest(byte[] buffer) throws IOException {
        this.buffer = buffer;
        marshaled = true;
    }

    @Override
    protected void marshalPrivate(ObjectOutputStream os) throws IOException {

    }

    @Override
    protected void unmarshalPrivate(ObjectInputStream is) throws IOException, ClassNotFoundException {

    }
}
