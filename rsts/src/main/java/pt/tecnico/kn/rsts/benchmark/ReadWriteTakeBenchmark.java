package pt.tecnico.kn.rsts.benchmark;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pt.tecnico.kn.rsts.data_type.Tuple;
import pt.tecnico.kn.rsts.network.RstsServer;
import pt.tecnico.kn.rsts.utils.TuplesGen;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.LongAdder;

/**
 * Benchmark concurrent read, write, take operation
 */
public class ReadWriteTakeBenchmark extends BaseBenchmark {

    private static final Logger Log = LoggerFactory.getLogger(ReadWriteTakeBenchmark.class);
    private static final Tuple SENTINEL = new Tuple("SENTINEL", "SENTINEL", "SENTINEL");
    private final Map<Tuple, LongAdder> takeCount = new ConcurrentHashMap<>();

    public ReadWriteTakeBenchmark(int serverCount, int clusterSize, int operationCount, String serverConfig) {
        super(serverCount, clusterSize, operationCount, serverConfig);
    }

    @Override
    public void doBench(RstsServer[] servers) throws InterruptedException {
        List<Tuple> tupleList = new ArrayList<>();
        TuplesGen.genSubset(3, 0, new HashSet<>(), TuplesGen.sources, tupleList);
        int totalWrite = tupleList.size() * servers.length;
        long startTime, endTime;
        ExecutorService executorService = Executors.newFixedThreadPool(servers.length);

        Log.info("Read-Write-Take benchmark started");
        startTime = new Date().getTime();
        System.out.printf("Start time: %d\n", startTime);
        // Write in
        for(Tuple t: tupleList) {
            for (RstsServer server : servers) {
                executorService.submit(() -> {
                    try {
                        server.write(t);
                    } catch (Exception e) {
                        Log.debug("Unable to write tuple");
                    }
                });
            }
        }

        for (RstsServer server : servers) {
            executorService.submit(() -> {
                // Write the SENTINEL tuple in
                Log.debug("Writing SENTINEL");
                try {
                    server.write(SENTINEL);
                } catch (Exception e) {
                    Log.debug("Unable to write tuple");
                }
                Log.debug("Finish writing tuples");
            });
        }


        // Read-Take out
        Random rd = new Random();
        int counter = 0;
        while(++counter <= operationCount / 2) {
            executorService.submit(() -> {
                RstsServer server = servers[rd.nextInt(servers.length)];
                Tuple pattern;
                do {
                    pattern = TuplesGen.getRandomTuple(TuplesGen.sources);
                } while (takeCount.containsKey(pattern) && takeCount.get(pattern).intValue() >= servers.length - 1);

                try {
                    Tuple out;
                    Log.debug("Taking: {}", pattern);
                    if (!takeCount.containsKey(pattern)) {
                        takeCount.put(pattern, new LongAdder());
                    }
                    takeCount.get(pattern).increment();
                    out = server.take(pattern);
                    Log.debug("Out: {}, Pattern: {}, match: {}", out, pattern, out.match(pattern));
                } catch (IOException | InterruptedException e) {
                    Log.debug("Error reading tuple", e);
                }
            });
        }

        while(++counter < operationCount) {
            executorService.submit(() -> {
                RstsServer server = servers[rd.nextInt(servers.length)];
                Tuple pattern = TuplesGen.getRandomTuple(TuplesGen.sources);
                try {
                    Log.debug("Reading: {}", pattern);
                    Tuple out = server.read(pattern);
                    Log.debug("Out: {}, Pattern: {}, match: {}", out, pattern, out.match(pattern));
                } catch (IOException | InterruptedException e) {
                    Log.debug("Error reading tuple", e);
                }
            });
        }

        // Taking the SENTINEL tuple out, because SENTINEL is put in last,
        // it's highly likely that all other tuples has finished writing.
        for (RstsServer server : servers) {
            executorService.submit(() -> {
                Log.debug("Taking SENTINEL");
                try {
                    Tuple out = server.take(SENTINEL);
                } catch (IOException | InterruptedException e) {
                    Log.debug("Error taking SENTINEL tuple");
                }
                Log.debug("SENTINEL taken");
            });
        }

        executorService.shutdown();
        executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);

        endTime = new Date().getTime();
        int totalTake = 0;
        for(LongAdder adder : takeCount.values()) {
            totalTake += adder.intValue();
        }

        int totalOperation = totalWrite + operationCount;

        System.out.printf("End time: %d\n", endTime);
        System.out.printf("Write operations' count: %d\n", totalWrite);
        System.out.printf("Take operations' count: %d\n", totalTake);
        System.out.printf("Read operations' count: %d\n", operationCount - totalTake);
        System.out.printf("Throughput: %f ops\n", (double) totalOperation * 1000 / (endTime - startTime));
        Log.info("Read-Write benchmark finished");
    }
}
