package pt.tecnico.kn.rsts.network;

import net.sf.jgcs.*;
import net.sf.jgcs.membership.BlockListener;
import net.sf.jgcs.membership.BlockSession;
import net.sf.jgcs.membership.MembershipListener;
import net.sf.jgcs.membership.MembershipSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.SocketAddress;
import java.util.List;

/**
 * Group control listener, also handle membership, exception and block
 */
public class GroupControlListener implements ControlListener, ExceptionListener, MembershipListener, BlockListener {
    private final static Logger Log = LoggerFactory.getLogger(GroupControlListener.class);
    private final ControlSession control;
    private final DataSession data;
    private final RstsServer server;
    private final int clusterSize;

    public GroupControlListener(ControlSession control, DataSession data, int clusterSize, RstsServer rstsServer) {
        this.control = control;
        this.data = data;
        this.server = rstsServer;
        this.clusterSize = clusterSize;
    }

    @Override
    public void onJoin(SocketAddress socketAddress) {
        Log.debug("{} joined", socketAddress);
    }

    @Override
    public void onLeave(SocketAddress socketAddress) {
        Log.debug("{} left", socketAddress);
        // After a server left, unlock all its lock
        server.getStorage().unlock(socketAddress);
    }

    @Override
    public void onFailed(SocketAddress socketAddress) {
        Log.debug("{} failed", socketAddress);
        // After a server failed, unlock all its lock
        server.getStorage().unlock(socketAddress);
    }

    @Override
    public void onException(JGCSException e) {

    }

    @Override
    public void onMembershipChange() {
        try {
            MembershipSession membershipSession = (MembershipSession) control;
            List<SocketAddress> members = membershipSession.getMembership().getMembershipList();
            Log.debug("-- NEW VIEW: {}, Size: {}",
                    membershipSession.getMembershipID(),
                    members.size());

            boolean wasPaused = server.isPause();

            if (members.size() <= clusterSize / 2) {
                Log.debug("Cluster too small, pausing server");
                server.setPause(true);
                return;
            } else {
                server.setPause(false);
            }

            server.viewChangeFinish();

            List<SocketAddress> joinedMembers = membershipSession.getMembership().getJoinedMembers();
            // No one join, exit
            if (joinedMembers == null) {
                return;
            }

            SocketAddress myAddress = membershipSession.getLocalAddress();
            // I was in the majority view
            if (!wasPaused) {
                boolean leader = true;
                List<SocketAddress> currentMembers = membershipSession.getMembership().getMembershipList();
                for(SocketAddress s: currentMembers) {
                    // Check if we're the leader
                    if (!joinedMembers.contains(s) && s.toString().compareTo(myAddress.toString()) > 0) {
                        leader = false;
                    }
                }
                // If I'm the leader, replicate data to all newly joined member
                if (leader) {
                    for(SocketAddress address: joinedMembers) {
                        Log.debug("Replicating data to: {}", address);
                        server.replicate(address);
                    }
                }
            }

            server.resentAllTakeRequest();
        } catch (NotJoinedException e) {
            Log.warn("Exception processing membership change request", e);
            data.close();
        } catch (IOException e) {
            Log.warn("Exception when replicating data to new node", e);
            data.close();
        } catch (InterruptedException e) {
            Log.warn("Exception when replicating tuples", e);
            data.close();
        }
    }

    @Override
    public void onExcluded() {

    }

    @Override
    public void onBlock() {
        Log.debug("Asking to BLOCK");
        try {
            server.setViewChangePending(true);
            ((BlockSession) control).blockOk();
        } catch (JGCSException e) {
            Log.warn("Exception when blocking", e);
        }
    }
}
