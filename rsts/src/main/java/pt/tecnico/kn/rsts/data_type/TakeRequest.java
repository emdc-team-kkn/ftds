package pt.tecnico.kn.rsts.data_type;

import java.io.*;

/**
 * Take message
 */
public class TakeRequest extends BaseMessage {
    private Tuple tuple;

    public TakeRequest(byte[] buffer) throws IOException {
        this.buffer = buffer;
        marshaled = true;
    }

    public TakeRequest(Tuple tuple) {
        this.command = MessageType.TAKE;
        this.tuple = tuple;
        marshaled = false;
    }

    @Override
    protected void marshalPrivate(ObjectOutputStream os) throws IOException {
        os.writeObject(tuple);
    }

    @Override
    protected void unmarshalPrivate(ObjectInputStream is) throws IOException, ClassNotFoundException {
        tuple = (Tuple) is.readObject();
    }

    public Tuple getTuple() {
        return tuple;
    }

    public void setTuple(Tuple tuple) {
        this.tuple = tuple;
    }

    @Override
    public String toString() {
        return "RstsMessage{" +
                "command=" + command +
                ", tuple=" + tuple +
                '}';
    }
}
