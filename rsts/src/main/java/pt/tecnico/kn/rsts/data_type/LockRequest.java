package pt.tecnico.kn.rsts.data_type;

import pt.tecnico.kn.rsts.storage.Key;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * This message is used in phase-2 of the take operation
 */
public class LockRequest extends BaseMessage {
    private Key key;
    private int round;

    public LockRequest(Key key, int round) {
        this.command = MessageType.LOCK;
        this.key = key;
        this.marshaled = false;
        this.round = round;
    }

    public LockRequest(byte[] buffer) throws IOException {
        super(buffer);
    }

    @Override
    protected void marshalPrivate(ObjectOutputStream os) throws IOException {
        os.writeObject(key);
        os.writeInt(round);
    }

    @Override
    protected void unmarshalPrivate(ObjectInputStream is) throws IOException, ClassNotFoundException {
        key = (Key) is.readObject();
        round = is.readInt();
    }

    public Key getKey() {
        return key;
    }

    public int getRound() {
        return round;
    }
}
