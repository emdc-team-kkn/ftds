package pt.tecnico.kn.rsts.data_type;

import pt.tecnico.kn.rsts.storage.Key;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.SocketAddress;

/**
 * Response to the lock request
 */
public class LockResponse extends BaseMessage {

    private boolean success;
    private Key key;
    private SocketAddress owner;
    private int round;

    public LockResponse(boolean success, Key key, SocketAddress owner, int round) {
        this.command = MessageType.LOCK_RESPONSE;
        this.success = success;
        this.key = key;
        this.marshaled = false;
        this.owner = owner;
        this.round = round;
    }

    public LockResponse(byte[] buffer) {
        super(buffer);
    }

    @Override
    protected void marshalPrivate(ObjectOutputStream os) throws IOException {
        os.writeBoolean(success);
        os.writeObject(key);
        os.writeObject(owner);
        os.writeInt(round);
    }

    @Override
    protected void unmarshalPrivate(ObjectInputStream is) throws IOException, ClassNotFoundException {
        success = is.readBoolean();
        key = (Key) is.readObject();
        owner = (SocketAddress) is.readObject();
        round = is.readInt();
    }

    public boolean isSuccess() {
        return success;
    }

    public Key getKey() {
        return key;
    }

    public SocketAddress getOwner() {
        return owner;
    }

    public int getRound() {
        return round;
    }
}
