package pt.tecnico.kn.rsts.data_type;

import pt.tecnico.kn.rsts.storage.Key;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Delete request
 */
public class DeleteRequest extends BaseMessage {

    private Key key;

    public DeleteRequest(Key key) {
        this.key = key;
        this.marshaled = false;
    }

    public DeleteRequest(byte[] buffer) {
        super(buffer);
    }

    @Override
    protected void marshalPrivate(ObjectOutputStream os) throws IOException {
        os.writeObject(key);
    }

    @Override
    protected void unmarshalPrivate(ObjectInputStream is) throws IOException, ClassNotFoundException {
        key = (Key) is.readObject();
    }

    public Key getKey() {
        return key;
    }
}
