package pt.tecnico.kn.rsts.data_type;

import pt.tecnico.kn.rsts.storage.Key;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

public class ReplicateRequest extends BulkMessage {
    public ReplicateRequest(byte[] buffer) throws IOException {
        super(buffer);
    }

    public ReplicateRequest(Set<Map.Entry<Key, Tuple>> tuples) {
        super(MessageType.REPLICATE, tuples);
    }
}
