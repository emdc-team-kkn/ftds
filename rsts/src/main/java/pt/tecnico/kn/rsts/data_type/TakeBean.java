package pt.tecnico.kn.rsts.data_type;

import pt.tecnico.kn.rsts.network.RstsServer;
import pt.tecnico.kn.rsts.storage.Key;

import java.net.SocketAddress;
import java.util.*;

/**
 * Bean object that compose of all data we need to store during a take
 */
public class TakeBean {
    private Set<SocketAddress> takeResponses = new HashSet<>();
    private Set<SocketAddress> lockResponses = new HashSet<>();
    private Set<SocketAddress> successLockResponses = new HashSet<>();
    private Map.Entry<Key, Tuple> candidate;
    private Set<Map.Entry<Key, Tuple>> proposals = new HashSet<>();
    private int view;
    private int lockRound = 0;

    public TakeBean(int view) {
        this.view = view;
    }

    public synchronized boolean collectTakeResponse(SocketAddress address, Collection<SocketAddress> check) {
        takeResponses.add(address);
        return takeResponses.containsAll(check);
    }

    public synchronized int takeResponsesSize() {
        return takeResponses.size();
    }

    public synchronized void clearTakeResponses() {
        takeResponses.clear();
    }

    public synchronized boolean collectLockResponse(SocketAddress address, boolean success, Collection<SocketAddress> check) {
        lockResponses.add(address);
        if (success) {
            successLockResponses.add(address);
        }
        return lockResponses.containsAll(check);
    }

    public synchronized boolean allLockSuccess() {
        return lockResponses.size() == successLockResponses.size();
    }

    public synchronized boolean majorityLockSuccess(int minimum) {
        return successLockResponses.size() > minimum;
    }

    public synchronized void clearLockResponses() {
        lockResponses.clear();
        successLockResponses.clear();
    }

    public synchronized void mergeProposals(Collection<Map.Entry<Key,Tuple>> others) {
        if (proposals.isEmpty()) {
            proposals.addAll(others);
        } else {
            proposals.retainAll(others);
        }
    }

    public synchronized Map.Entry<Key, Tuple> getCandidate() {
        return candidate;
    }

    public synchronized Set<Map.Entry<Key, Tuple>> getProposals() {
        return proposals;
    }

    public synchronized void setCandidate(Map.Entry<Key, Tuple> candidate) {
        this.candidate = candidate;
    }

    public int getView() {
        return view;
    }

    public void setView(int view) {
        this.view = view;
    }

    public int getLockRound() {
        return lockRound;
    }

    public void setLockRound(int lockRound) {
        this.lockRound = lockRound;
    }
}
