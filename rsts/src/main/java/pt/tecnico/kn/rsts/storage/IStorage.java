package pt.tecnico.kn.rsts.storage;

import pt.tecnico.kn.rsts.data_type.Tuple;

import java.net.SocketAddress;
import java.util.Map;
import java.util.Set;

/**
 * Interface for our storage
 */
public interface IStorage {
    void write(Key key, Tuple out);

    void write(Set<Map.Entry<Key, Tuple>> out);

    Map.Entry<Key, Tuple> readFirst(Tuple pattern) throws InterruptedException;

    Set<Map.Entry<Key, Tuple>> readAll();

    int getSize();

    Set<Map.Entry<Key, Tuple>> takeAll(Tuple pattern, Lock lock) throws InterruptedException;

    void delete(Key key);

    boolean lock(Key key, Lock requester);

    void unlock(Lock lock);

    void unlock(SocketAddress sock);

    void unlock(String cookies);

    void drop();

    void printStatus();
}
