package pt.tecnico.kn.rsts.network;

import java.io.IOException;

/**
 * This exception is throw when an operation is perform with not enough peer
 */
public class ServerPauseException extends IOException {
    private static final long serialVersionUID = -478488652909193839L;
}
