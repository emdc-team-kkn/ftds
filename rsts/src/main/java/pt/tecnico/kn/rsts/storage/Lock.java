package pt.tecnico.kn.rsts.storage;

import java.net.SocketAddress;
import java.util.Objects;

/**
 * We use this to lock a tuple for taking
 */
public class Lock {
    private final String cookies;
    private final SocketAddress address;

    public Lock(String cookies, SocketAddress address) {
        this.cookies = cookies;
        this.address = address;
    }

    public String getCookies() {
        return cookies;
    }

    public SocketAddress getAddress() {
        return address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Lock lock = (Lock) o;
        return Objects.equals(cookies, lock.cookies) &&
                Objects.equals(address, lock.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cookies, address);
    }

    @Override
    public String toString() {
        return "Lock{" +
                "cookies='" + cookies + '\'' +
                ", address=" + address +
                '}';
    }
}
