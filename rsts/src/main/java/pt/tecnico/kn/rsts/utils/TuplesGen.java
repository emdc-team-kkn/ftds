package pt.tecnico.kn.rsts.utils;

import pt.tecnico.kn.rsts.data_type.Tuple;

import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * Utils class to generate tuples
 */
public class TuplesGen {
    private static Random rd = new Random();

    public static final String[] sources = {
            "schtroumpfed", "squirrelled", "broughammed", "schmaltzed", "squirreled",
            "scrootched", "scroonched", "scraunched", "broughamed", "strengthed",
            "schwartzed", "laugharne", "schmertzed", "scratcheds", "scroungeds",
            "instituto", "superior", "tecnico"
    };

    public static Tuple getRandomTuple(String[] in) {
        String s1, s2;
        Tuple pattern;
        do {
            s1 = in[rd.nextInt(in.length)];
            s2 = in[rd.nextInt(in.length)];
            int combo = rd.nextInt(3);
            switch (combo) {
                case 1:
                    pattern = new Tuple(s1, s2, "*");
                    break;
                case 2:
                    pattern = new Tuple(s1, "*", s2);
                    break;
                case 0:
                default:
                    pattern = new Tuple("*", s1, s2);
                    break;
            }
        } while (s2.equals(s1));
        return pattern;
    }

    public static void genSubset(int left, int index, Set<String> acc, String[] in, List<Tuple> out) {
        if (left == 0) {
            String[] output = new String[acc.size()];
            acc.toArray(output);
            genPermutation(output, output.length, out);
        } else {
            for (int i = index; i < in.length; i++) {
                acc.add(in[i]);
                genSubset(left - 1, i + 1, acc, in, out);
                acc.remove(in[i]);
            }
        }
    }

    public static void genPermutation(String[] source, int left, List<Tuple> out) {
        if (left == 1) {
            Tuple t = new Tuple(source[0], source[1], source[2]);
            out.add(t);
        } else {
            String tmp;
            for (int i = 0; i < left; i++) {
                genPermutation(source, left - 1, out);
                if (left % 2 == 0) {
                    tmp = source[i];
                    source[i] = source[left - 1];
                    source[left - 1] = tmp;
                } else {
                    tmp = source[0];
                    source[0] = source[left - 1];
                    source[left - 1] = tmp;
                }
            }
        }
    }
}
