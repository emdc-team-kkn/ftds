package pt.tecnico.kn.rsts.data_type;

import pt.tecnico.kn.rsts.storage.Key;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

public class TakeResponse extends BulkMessage {
    public TakeResponse(byte[] buffer) throws IOException {
        super(buffer);
    }

    public TakeResponse(Set<Map.Entry<Key, Tuple>> tuples) {
        super(MessageType.TAKE_RESPONSE, tuples);
    }
}
