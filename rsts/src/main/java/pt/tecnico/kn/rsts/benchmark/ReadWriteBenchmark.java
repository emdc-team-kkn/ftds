package pt.tecnico.kn.rsts.benchmark;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pt.tecnico.kn.rsts.data_type.Tuple;
import pt.tecnico.kn.rsts.network.RstsServer;
import pt.tecnico.kn.rsts.utils.TuplesGen;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Read-Write benchmark
 */
public class ReadWriteBenchmark extends BaseBenchmark {

    private static final Logger Log = LoggerFactory.getLogger(ReadWriteBenchmark.class);

    public ReadWriteBenchmark(int serverCount, int clusterSize, int operationCount, String serverConfig) {
        super(serverCount, clusterSize, operationCount, serverConfig);
    }

    @Override
    public void doBench(RstsServer[] servers) throws InterruptedException {
        List<Tuple> tupleList = new ArrayList<>();
        TuplesGen.genSubset(3, 0, new HashSet<>(), TuplesGen.sources, tupleList);
        int totalWrite = tupleList.size() * servers.length;
        int totalRead = operationCount;
        long startTime, endTime;
        ExecutorService executorService = Executors.newFixedThreadPool(servers.length * 10);

        Log.info("Read-Write benchmark started");
        startTime = new Date().getTime();
        System.out.printf("Start time: %d\n", startTime);
        // Write in
        for (RstsServer server : servers) {
            executorService.submit(() -> {
                for(Tuple t: tupleList) {
                    try {
                        server.write(t);
                    } catch (Exception e) {
                        Log.debug("Unable to write tuple");
                    }
                }
                // Wait till server receive all write
                synchronized (server) {
                    while (server.getStorage().getSize() < totalWrite) {
                        Log.debug("Item stored: {}", server.getStorage().getSize());
                        try {
                            server.wait(500);
                        } catch (InterruptedException e) {
                            Log.debug("Unable to sleep", e);
                        }
                    }
                }
            });
        }

        // Read out
        Random rd = new Random();
        int counter = 0;
        while(++counter < operationCount) {
            executorService.submit(() -> {
                RstsServer server = servers[rd.nextInt(servers.length)];
                Tuple pattern = TuplesGen.getRandomTuple(TuplesGen.sources);
                try {
                    Tuple out = server.read(pattern);
                } catch (IOException | InterruptedException e) {
                    Log.debug("Error reading tuple", e);
                }
            });
        }

        executorService.shutdown();
        executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);

        endTime = new Date().getTime();
        int totalOperation = totalWrite + totalRead;
        System.out.printf("End time: %d\n", endTime);
        System.out.printf("Operations' count: %d\n", totalOperation);
        System.out.printf("Throughput: %f ops\n", (double) totalOperation * 1000 / (endTime - startTime));
        Log.info("Read-Write benchmark finished");
    }
}
