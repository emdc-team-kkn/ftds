package pt.tecnico.kn.rsts.data_type;

/**
 * List of command supported by our tuple space implementation
 */
public enum MessageType {
    WRITE,
    REPLICATE,
    TAKE, TAKE_RESPONSE,
    LOCK, LOCK_RESPONSE
}
