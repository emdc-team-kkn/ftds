package pt.tecnico.kn.rsts.benchmark;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pt.tecnico.kn.rsts.data_type.Tuple;
import pt.tecnico.kn.rsts.network.RstsServer;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Benchmark write operation
 */
public class WriteBenchmark extends BaseBenchmark {
    private final static Logger Log = LoggerFactory.getLogger(WriteBenchmark.class);

    public WriteBenchmark(int serverCount, int clusterSize, int operationCount, String serverConfig) {
        super(serverCount, clusterSize, operationCount, serverConfig);
    }

    @Override
    public void doBench(RstsServer[] servers) throws InterruptedException {
        long startTime, endTime;
        int totalOperation = servers.length * operationCount;

        ExecutorService executorService = Executors.newFixedThreadPool(servers.length);
        Log.info("Write benchmark started");

        startTime = new Date().getTime();
        System.out.printf("Start time: %d\n", startTime);
        for (RstsServer server : servers) {
            executorService.submit(() -> {
                int counter = 0;
                while (++counter <= operationCount) {
                    try {
                        server.write(new Tuple("s1", "s2", "s3"));
                    } catch (Exception e) {
                        Log.debug("Unable to write tuple");
                    }
                }
                // Wait till server receive all write
                synchronized (server) {
                    while (server.getStorage().getSize() < totalOperation) {
                        Log.debug("Item stored: {}", server.getStorage().getSize());
                        try {
                            server.wait(500);
                        } catch (InterruptedException e) {
                            Log.debug("Unable to sleep", e);
                        }
                    }
                }
            });
        }
        // Wait till execution is complete
        executorService.shutdown();
        executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);

        endTime = new Date().getTime();
        System.out.printf("End time: %d\n", endTime);
        System.out.printf("Operations' count: %d\n", totalOperation);
        System.out.printf("Throughput: %f ops\n", (double) totalOperation * 1000 / (endTime - startTime));
        Log.info("Write benchmark finished");
    }
}
