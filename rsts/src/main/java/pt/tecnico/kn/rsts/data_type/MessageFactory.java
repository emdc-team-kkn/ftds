package pt.tecnico.kn.rsts.data_type;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Optional;

/**
 * Factory class to create message.
 */
public class MessageFactory {
    public static Optional<BaseMessage> GetMessage(byte[] buffer) throws IOException, ClassNotFoundException {
        ByteArrayInputStream bis = new ByteArrayInputStream(buffer);
        ObjectInputStream is = new ObjectInputStream(bis);
        MessageType command = (MessageType) is.readObject();
        is.close();
        bis.close();
        switch (command) {
            case WRITE:
                return Optional.of(new WriteRequest(buffer));
            case REPLICATE:
                return Optional.of(new ReplicateRequest(buffer));
            case TAKE_RESPONSE:
                return Optional.of(new TakeResponse(buffer));
            case LOCK:
                return Optional.of(new LockRequest(buffer));
            case LOCK_RESPONSE:
                return Optional.of(new LockResponse(buffer));
            case TAKE:
                return Optional.of(new TakeRequest(buffer));
            default:
                return Optional.empty();
        }
    }
}
