package pt.tecnico.kn.rsts.network;

import net.sf.jgcs.*;
import net.sf.jgcs.membership.MembershipSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pt.tecnico.kn.rsts.data_type.*;
import pt.tecnico.kn.rsts.storage.Key;
import pt.tecnico.kn.rsts.storage.Lock;

import java.io.IOException;
import java.net.SocketAddress;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * This class represent a group message listener.
 */
public class GroupMessageListener implements MessageListener, ServiceListener {
    private static final Logger Log = LoggerFactory.getLogger(GroupMessageListener.class);
    private final DataSession groupDataSession;
    private final Service groupService;
    private final RstsServer server;
    private final ExecutorService executorService;
    private final ControlSession controlSession;
    private final Map<String, TakeBean> currentTakes = new ConcurrentHashMap<>();
    private final int clusterSize;
    private final Random random;

    public GroupMessageListener(Service groupService, ControlSession controlSession, DataSession groupDataSession, int clusterSize, RstsServer server) {
        this.groupService = groupService;
        this.groupDataSession = groupDataSession;
        this.server = server;
        this.executorService = Executors.newCachedThreadPool();
        this.controlSession = controlSession;
        this.clusterSize = clusterSize;
        this.random = new Random();
    }

    @Override
    public void onServiceEnsured(Object o, Service service) {
        // Do nothing
    }

    @Override
    public Object onMessage(Message message) {
        if (server.isPause()) {
            return null;
        }
        try {
            Optional<BaseMessage> baseMessage = MessageFactory.GetMessage(message.getPayload());
            baseMessage.ifPresent(m -> processMessage(m, message.getSenderAddress()));
        } catch (IOException | ClassNotFoundException e) {
            Log.warn("Exception on parsing message", e);
        }
        return null;
    }

    private void processMessage(BaseMessage message, SocketAddress address) {
        try {
            message.unmarshal();
            switch (message.getCommand()) {
                case WRITE:
                    processWriteRequest((WriteRequest) message);
                    break;
                case REPLICATE:
                    processReplicateRequest((ReplicateRequest) message);
                    break;
                case TAKE:
                    processTakeRequest((TakeRequest) message, address);
                    break;
                case TAKE_RESPONSE:
                    processTakeResponse((TakeResponse) message, address);
                    break;
                case LOCK:
                    processLockRequest((LockRequest) message, address);
                    break;
                case LOCK_RESPONSE:
                    processLockResponse((LockResponse) message, address);
                    break;
            }
        } catch (IOException | ClassNotFoundException e) {
            Log.warn("Exception on parsing message", e);
        }
    }

    private void processTakeRequest(TakeRequest message, SocketAddress address) {
        executorService.submit(() -> {
            Tuple pattern = message.getTuple();
            String cookies = message.getCookies();
            try {
                currentTakes.putIfAbsent(cookies, new TakeBean(server.getView()));
                TakeBean bean = currentTakes.get(cookies);

                synchronized (bean) {
                    if(server.getView() > bean.getView()) {
                        // View has changed, update the bean
                        bean.setView(server.getView());
                        bean.clearTakeResponses();
                    } else if (server.getView() < bean.getView()) {
                        // Take request already restarted, nothing to do here
                        return;
                    }
                }

                // Take & lock all unlocked tuple that matches the pattern
                Set<Map.Entry<Key, Tuple>> result = server.getStorage()
                        .takeAll(pattern, new Lock(cookies, address));
                // Send back the result to the requester
                BaseMessage response = new TakeResponse(result);
                response.setCookies(cookies);
                // TODO: Figure out why message send to self never arrive
                Log.debug("Receving take request for: {}", cookies);
                synchronized (server) {
                    while(server.isViewChangePending()) {
                        server.wait();
                    }
                    if (address.equals(controlSession.getLocalAddress())) {
                        processTakeResponse((TakeResponse) response, address);
                    } else {
                        response.marshal();
                        Message responseMessage = groupDataSession.createMessage();
                        responseMessage.setPayload(response.getPayload());
                        groupDataSession.send(responseMessage, groupService, null, address);
                    }
                }
            } catch (InterruptedException e) {
                Log.debug("Exception when searching tuple", e);
            } catch (IOException e) {
                Log.debug("Unable to send response", e);
            }
        });
    }

    private void processTakeResponse(TakeResponse message, SocketAddress address) {
        MembershipSession membershipSession = (MembershipSession) controlSession;
        executorService.submit(() -> {
            try {
                Log.debug("Receving take response for: {}", message.getCookies());
                Log.debug("Address: {}, from: {}", controlSession.getLocalAddress(), address);
                String cookies = message.getCookies();
                currentTakes.putIfAbsent(cookies, new TakeBean(server.getView()));
                TakeBean bean = currentTakes.get(cookies);

                synchronized (bean) {
                    if(server.getView() > bean.getView()) {
                        // View has changed, update the bean, then send take request again
                        bean.setView(server.getView());
                        bean.clearTakeResponses();
                    } else if (server.getView() < bean.getView()) {
                        // Take request already restarted, nothing to do here
                        return;
                    }
                }

                // And the response count
                bean.mergeProposals(message.getTuples());
                boolean allCollected = bean.collectTakeResponse(
                        address,
                        membershipSession.getMembership().getMembershipList()
                );

                if (allCollected) {
                    Set<Map.Entry<Key, Tuple>> results = bean.getProposals();
                    if (results.isEmpty()) {
                        // We were not be able to select a tuple, retry
                        Log.debug("Unable to find matched for tuple. Retrying...");
                        bean.clearTakeResponses();
                        server.resendTakeRequest(cookies);
                    } else {
                        // We have collect response from everyone
                        // Take the one tuple and delete it from all node
                        Map.Entry<Key, Tuple> result = results.stream().findFirst().get();
                        Log.debug("Result to pick: {}", result);
                        // Lock the tuple for taking
                        Key key = result.getKey();
                        bean.setCandidate(result);
                        server.sendLockRequest(cookies, key, bean.getLockRound() + 1);
                    }
                }
            } catch (Exception e) {
                Log.debug("Exception", e);
            }
        });
    }

    private void processLockRequest(LockRequest message, SocketAddress address) {
        executorService.submit(() -> {
            try {
                String cookies = message.getCookies();
                Log.debug("Receive lock request for {}", cookies);
                Lock l = new Lock(cookies, address);
                // Lock the tuple
                boolean lockSuccess = server.getStorage().lock(message.getKey(), l);
                BaseMessage response = new LockResponse(
                        lockSuccess, message.getKey(), address, message.getRound()
                );
                response.setCookies(cookies);

                synchronized (server) {
                    while(server.isViewChangePending()) {
                        server.wait();
                    }
                    // Broadcast the lock response to everyone
                    response.marshal();
                    Message responseMessage = groupDataSession.createMessage();
                    responseMessage.setPayload(response.getPayload());
                    groupDataSession.multicast(responseMessage, groupService, null);
                }
            } catch (Exception e) {
                Log.debug("Exception", e);
            }
        });
    }

    private void processLockResponse(LockResponse message, SocketAddress address) {
        MembershipSession membershipSession = (MembershipSession) controlSession;
        executorService.submit(() -> {
            try {
                String cookies = message.getCookies();
                Log.debug("Receive lock response for {}", cookies);
                // Increase response count
                TakeBean bean = currentTakes.get(cookies);
                if (bean == null) {
                    // A lock response arrive after the take has finished, ignore
                    return;
                }
                Log.debug("Lock round: {}, old round: {}", message.getRound(), bean.getLockRound());
                Log.debug("Original view: {}, Current view: {}", bean.getView(), server.getView());
                synchronized (bean) {
                    if(server.getView() != bean.getView()) {
                        // Ignore all lock response from a take message in the old view
                        return;
                    }
                    if (message.getRound() > bean.getLockRound()) {
                        bean.clearLockResponses();
                        bean.setLockRound(message.getRound());
                    } else if (message.getRound() < bean.getLockRound()) {
                        // This is a lock response from a previous round
                        return;
                    }
                }

                boolean allCollected = bean.collectLockResponse(
                        address, message.isSuccess(),
                        membershipSession.getMembership().getMembershipList()
                );

                if (allCollected) {
                    if (bean.allLockSuccess()) {
                        // Get success lock response from everyone
                        currentTakes.remove(cookies);
                        if (message.getOwner().equals(controlSession.getLocalAddress())) {
                            Log.debug("Got majority, address: {}, cookies: {}, tuple: {}", controlSession.getLocalAddress(), cookies, bean.getCandidate());
                            // If I am the owner, push the result to the client
                            Map.Entry<Key, Tuple> result = bean.getCandidate();
                            server.addResult(cookies, result.getValue());
                            server.removePendingTake(cookies);
                        }
                        // Delete the item
                        Log.debug("Delete item for: {}", message.getCookies());
                        Key key = message.getKey();
                        server.getStorage().delete(key);
                        server.getStorage().unlock(message.getCookies());
                    } else if (bean.majorityLockSuccess(clusterSize / 2)) {
                        if (message.getOwner().equals(controlSession.getLocalAddress())) {
                            // If I am the owner, retry to acquire lock again
                            server.sendLockRequest(cookies, message.getKey(), bean.getLockRound() + 1);
                        }
                    } else {
                        Lock l = new Lock(message.getCookies(), address);
                        server.getStorage().unlock(l);
                        if (message.getOwner().equals(controlSession.getLocalAddress())) {
                            // If I am the owner, release lock, retry from step 1
                            bean.clearTakeResponses();
                            // Sleep a random time
                            long sleepTime = random.nextInt(10)  * 50;
                            Thread.sleep(sleepTime);
                            // Try again
                            server.resendTakeRequest(cookies);
                        }
                    }
                }
            } catch (Exception e) {
                Log.warn("Exception when processing request", e);
            }
        });
    }

    private void processWriteRequest(WriteRequest message) {
        executorService.submit(() -> {
            server.getStorage().write(message.getKey(), message.getTuple());
        });
    }

    private void processReplicateRequest(ReplicateRequest message) {
        executorService.submit(() -> {
            // Drop all of our data when we receive a replicate request
            server.dropData();
            server.getStorage().write(message.getTuples());
            // Update the max counter of the server
            AtomicInteger maxCounter = server.getCounter();
            for(Map.Entry<Key, Tuple> item: message.getTuples()) {
                if (item.getKey().getSocketAddress().equals(controlSession.getLocalAddress())) {
                    if (item.getKey().getCounter() > maxCounter.intValue()) {
                        maxCounter.set(item.getKey().getCounter());
                    }
                }
            }
        });
    }
}
