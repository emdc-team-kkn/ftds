package pt.tecnico.kn.rsts.data_type;

import pt.tecnico.kn.rsts.storage.Key;

import java.io.*;
import java.util.*;

/**
 * This class represent a message use in bulk operation with multiple Tuple
 */
public class BulkMessage extends BaseMessage {
    private Set<Map.Entry<Key, Tuple>> tuples;

    public BulkMessage(byte[] buffer) throws IOException {
        super(buffer);
    }

    public BulkMessage(MessageType command, Set<Map.Entry<Key, Tuple>> tuples) {
        this.command = command;
        this.tuples = tuples;
        this.marshaled = false;
    }

    @Override
    protected void marshalPrivate(ObjectOutputStream os) throws IOException {
        os.writeInt(tuples.size());
        for(Map.Entry<Key, Tuple> t: tuples) {
            os.writeObject(t.getKey());
            os.writeObject(t.getValue());
        }
    }

    @Override
    protected void unmarshalPrivate(ObjectInputStream is) throws IOException, ClassNotFoundException {
        int listSize = is.readInt();
        tuples = new HashSet<>(listSize);
        for(int i = 0; i < listSize; i++) {
            tuples.add(new AbstractMap.SimpleEntry<>((Key) is.readObject(), (Tuple) is.readObject()));
        }
    }

    public Set<Map.Entry<Key, Tuple>> getTuples() {
        return tuples;
    }

    @Override
    public String toString() {
        return "RstsBulkMessage{" +
                "command=" + command +
                ", tuples=" + tuples +
                '}';
    }
}
