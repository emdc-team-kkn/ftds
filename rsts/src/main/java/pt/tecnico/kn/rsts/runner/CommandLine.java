package pt.tecnico.kn.rsts.runner;

import asg.cliche.Command;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pt.tecnico.kn.rsts.data_type.Tuple;
import pt.tecnico.kn.rsts.network.ServerPauseException;
import pt.tecnico.kn.rsts.network.RstsServer;
import pt.tecnico.kn.rsts.storage.IStorage;

import java.io.IOException;

/**
 * This is our command line interface
 */
public class CommandLine {

    private final static Logger Log = LoggerFactory.getLogger(CommandLine.class);
    private final RstsServer server;

    public CommandLine(RstsServer server) {
        this.server = server;
    }

    @Command
    public void write(String s1, String s2, String s3) {
        try {
            server.write(new Tuple(s1, s2, s3));
        } catch (ServerPauseException e) {
            System.out.println("Your cluster is too small. Increase the number of server");
        } catch (Exception e) {
            System.out.println("Unable to write new tuple");
            Log.debug("Exception when writing new tuple", e);
        }
    }

    @Command
    public void read(String s1, String s2, String s3) {
        try {
            Tuple tuple = server.read(new Tuple(s1, s2, s3));
            System.out.println("Tuple: " + tuple);
        } catch (ServerPauseException e) {
            System.out.println("Your cluster is too small. Increase the number of server");
        } catch (InterruptedException | IOException e) {
            System.out.println("Unable to read tuple");
            Log.debug("Exception when reading tuple", e);
        }
    }

    @Command
    public void take(String s1, String s2, String s3) {
        try {
            Tuple tuple = server.take(new Tuple(s1, s2, s3));
            System.out.println("Tuple: " + tuple);
        } catch (ServerPauseException e) {
            System.out.println("Your cluster is too small. Increase the number of server");
        } catch (IOException | InterruptedException e) {
            System.out.println("Unable to take tuple");
            Log.debug("Exception when reading tuple", e);
        }
    }

    @Command
    public void status() {
        server.printStatus();

    }
}
