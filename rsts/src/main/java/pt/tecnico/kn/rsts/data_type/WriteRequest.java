package pt.tecnico.kn.rsts.data_type;

import pt.tecnico.kn.rsts.storage.Key;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * This is a write message
 */
public class WriteRequest extends BaseMessage {

    private Key key;
    private Tuple tuple;

    public WriteRequest(Key key, Tuple tuple) {
        this.tuple = tuple;
        this.key = key;
        this.command = MessageType.WRITE;
        this.marshaled = false;
    }

    public WriteRequest(byte[] buffer) {
        super(buffer);
    }

    @Override
    protected void marshalPrivate(ObjectOutputStream os) throws IOException {
        os.writeObject(key);
        os.writeObject(tuple);
    }

    @Override
    protected void unmarshalPrivate(ObjectInputStream is) throws IOException, ClassNotFoundException {
        key = (Key) is.readObject();
        tuple = (Tuple) is.readObject();
    }

    public Key getKey() {
        return key;
    }

    public Tuple getTuple() {
        return tuple;
    }
}
