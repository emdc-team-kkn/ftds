package pt.tecnico.kn.rsts.network;

import net.sf.appia.jgcs.AppiaGroup;
import net.sf.appia.jgcs.AppiaProtocolFactory;
import net.sf.appia.jgcs.AppiaService;
import net.sf.jgcs.*;
import net.sf.jgcs.membership.BlockSession;
import net.sf.jgcs.membership.MembershipSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pt.tecnico.kn.rsts.data_type.*;
import pt.tecnico.kn.rsts.storage.IStorage;
import pt.tecnico.kn.rsts.storage.Key;

import java.io.IOException;
import java.net.SocketAddress;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * This class represent a server in out RSTS group
 */
public class RstsServer {
    private static final Logger Log = LoggerFactory.getLogger(RstsServer.class);

    private final String serverConfig;
    private final String name;
    private final IStorage storage;
    private DataSession dataSession;
    private ControlSession controlSession;
    private Service groupService;
    private Map<String, Tuple> resultStore;
    private Map<String, Tuple> requestStore;
    private Set<String> pendingTakes;
    private boolean pause;
    private boolean viewChangePending;
    private AtomicInteger counter;
    private int view;
    private ExecutorService executorService;
    private AtomicInteger cookiesCounter;

    public RstsServer(String name, String serverConfig, IStorage storage) {
        this.name = name;
        this.serverConfig = serverConfig;
        this.storage = storage;
        this.resultStore = new HashMap<>();
        this.requestStore = new ConcurrentHashMap<>();
        this.pendingTakes = new HashSet<>();
        this.pause = false;
        this.counter = new AtomicInteger(0);
        this.cookiesCounter = new AtomicInteger(0);
        this.viewChangePending = false;
        this.view = 1;
        this.executorService = Executors.newCachedThreadPool();
    }

    public void start(int clusterSize) throws JGCSException {
        Log.debug("RSTS server is starting");
        AppiaGroup appiaGroup = new AppiaGroup();
        appiaGroup.setGroupName(name);
        appiaGroup.setConfigFileName(serverConfig);
        ProtocolFactory protocolFactory = new AppiaProtocolFactory();
        Protocol rstsProtocol = protocolFactory.createProtocol();
        dataSession = rstsProtocol.openDataSession(appiaGroup);
        controlSession = rstsProtocol.openControlSession(appiaGroup);
        groupService = new AppiaService("rsts_group");

        GroupMessageListener messageListener = new GroupMessageListener(groupService, controlSession, dataSession, clusterSize, this);
        GroupControlListener controlListener = new GroupControlListener(controlSession, dataSession, clusterSize, this);
        dataSession.setMessageListener(messageListener);
        dataSession.setServiceListener(messageListener);
        // Control listener
        controlSession.setControlListener(controlListener);
        controlSession.setExceptionListener(controlListener);
        if (controlSession instanceof MembershipSession) {
            ((MembershipSession) controlSession).setMembershipListener(controlListener);
        }
        if (controlSession instanceof BlockSession) {
            ((BlockSession) controlSession).setBlockListener(controlListener);
        }
        controlSession.join();
        Log.debug("RSTS server is started");
    }

    public void stop() throws JGCSException {
        if (controlSession.isJoined()) {
            controlSession.leave();
        }
    }

    public boolean write(Tuple out) throws IOException, InterruptedException {
        if (pause) {
            throw new ServerPauseException();
        }
        Key key = new Key(counter.incrementAndGet(), controlSession.getLocalAddress());
        BaseMessage message = new WriteRequest(key, out);
        try {
            Message m = dataSession.createMessage();
            message.marshal();
            m.setPayload(message.getPayload());
            synchronized (this) {
                while(viewChangePending) {
                    wait();
                }
                dataSession.multicast(m, groupService, null);
            }
        } catch (ClosedSessionException | UnsupportedServiceException e) {
            Log.warn("Exception when writing tuple", e);
            throw new IOException("Exception when writing tuple", e);
        }
        return true;
    }

    public Tuple read(Tuple pattern) throws IOException, InterruptedException {
        Log.debug("Reading tuple matching {} from storage", pattern);
        return storage.readFirst(pattern).getValue();
    }

    public Tuple take(Tuple pattern) throws IOException, InterruptedException {
        if (pause) {
            throw new ServerPauseException();
        }
        String cookies = String.format("%s|%d", controlSession.getLocalAddress(), cookiesCounter.incrementAndGet());
        requestStore.put(cookies, pattern);
        sendTakeRequest(cookies, pattern);
        Tuple result;
        synchronized (this) {
            while ((result = resultStore.get(cookies)) == null) {
                wait();
            }
        }
        // Delete the taken result from the result store
        resultStore.remove(cookies);
        return result;
    }

    public void dropData() {
        storage.drop();
    }

    public void replicate(SocketAddress destination) throws IOException, InterruptedException {
        int storageSize = storage.getSize();
        if (storageSize == 0) {
            return;
        }
        Set<Map.Entry<Key, Tuple>> allTuples = storage.readAll();
        BaseMessage message = new BulkMessage(MessageType.REPLICATE, allTuples);
        try {
            Message m = dataSession.createMessage();
            message.marshal();
            m.setPayload(message.getPayload());
            synchronized (this) {
                while(viewChangePending) {
                    wait();
                }
                dataSession.send(m, groupService, null, destination);
            }
        } catch (IOException e) {
            Log.warn("Error when replicating data to {}", destination);
            throw new IOException("Exception when replicating data", e);
        }
    }

    public IStorage getStorage() {
        return storage;
    }

    public synchronized void addResult(String cookies, Tuple result) {
        resultStore.put(cookies, result);
        notifyAll();
    }

    public Tuple getRequestPattern(String cookies) {
        return requestStore.get(cookies);
    }

    public boolean isPause() {
        return pause;
    }

    public synchronized void setPause(boolean pause) {
        this.pause = pause;
    }

    public boolean isViewChangePending() {
        return viewChangePending;
    }

    public synchronized void setViewChangePending(boolean viewChangePending) {
        this.viewChangePending = viewChangePending;
        notifyAll();
    }

    public synchronized void viewChangeFinish() {
        viewChangePending = false;
        view++;
    }

    public int getView() {
        return view;
    }

    public void sendTakeRequest(String cookies, Tuple pattern) throws IOException, InterruptedException {
        BaseMessage message = new TakeRequest(pattern);
        message.setCookies(cookies);
        Message m = dataSession.createMessage();
        message.marshal();
        m.setPayload(message.getPayload());
        // Wait until we have a matched in our local storage
        // to save network bandwidth
        getStorage().readFirst(pattern);

        synchronized (this) {
            while(isViewChangePending()) {
                wait();
            }
            if (!pendingTakes.contains(cookies)) {
                pendingTakes.add(cookies);
                dataSession.multicast(m, groupService, null);
            }
        }
    }

    public void sendLockRequest(String cookies, Key key, int round) throws IOException, InterruptedException {
        BaseMessage takeP2 = new LockRequest(key, round);
        takeP2.setCookies(cookies);
        takeP2.marshal();
        Message take2Message = dataSession.createMessage();
        take2Message.setPayload(takeP2.getPayload());
        synchronized (this) {
            while(isViewChangePending()) {
                wait();
            }
            dataSession.multicast(take2Message, groupService, null);
        }
    }

    public void resentAllTakeRequest() {
        synchronized (this) {
            pendingTakes.clear();
        }
        for(Map.Entry<String, Tuple> entry: requestStore.entrySet()) {
            executorService.submit(() -> {
                try {
                    sendTakeRequest(entry.getKey(), entry.getValue());
                } catch (Exception e) {
                    Log.debug("Unable to issue take command", e);
                }
            });
        }
    }

    public Tuple removePendingTake(String cookies) {
        return requestStore.remove(cookies);
    }

    public AtomicInteger getCounter() {
        return counter;
    }

    public void printStatus() {
        System.out.printf("Network address: %s%n", controlSession.getLocalAddress());
        System.out.println("Group members: ");
        MembershipSession membershipSession = (MembershipSession) controlSession;
        try {
            for(SocketAddress member : membershipSession.getMembership().getMembershipList()) {
                System.out.println(member);
            }
        } catch (NotJoinedException e) {
            Log.debug("Unable to get membership", e);
        }
        storage.printStatus();
    }

    public void resendTakeRequest(String cookies) throws IOException, InterruptedException {
        synchronized (this) {
            pendingTakes.remove(cookies);
        }
        sendTakeRequest(cookies, requestStore.get(cookies));
    }
}
