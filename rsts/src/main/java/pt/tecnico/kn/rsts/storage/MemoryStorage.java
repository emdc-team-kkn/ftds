package pt.tecnico.kn.rsts.storage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pt.tecnico.kn.rsts.data_type.Tuple;

import java.net.SocketAddress;
import java.util.*;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * In memory tuple storage
 */
public class MemoryStorage implements IStorage {

    private static final Logger Log = LoggerFactory.getLogger(MemoryStorage.class);
    private Map<Key, Tuple> tuples = new HashMap<>();
    private Map<Key, Lock> takeLock = new HashMap<>();
    private final ReentrantLock lock = new ReentrantLock(true);
    private final Condition newItem = lock.newCondition();

    @Override
    public void write(Key key, Tuple out) {
        Log.debug("Writing tuple: {}", out);
        lock.lock();
        try {
            tuples.put(key, out);
            newItem.signalAll();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void write(Set<Map.Entry<Key, Tuple>> out) {
        Log.debug("Writing {} tuples", out.size());
        lock.lock();
        try {
            for(Map.Entry<Key, Tuple> t: out) {
                tuples.put(t.getKey(), t.getValue());
            }
            newItem.signalAll();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public Map.Entry<Key, Tuple> readFirst(Tuple pattern) throws InterruptedException {
        lock.lock();
        try {
            Optional<Map.Entry<Key, Tuple>> result = tuples.entrySet().stream()
                    .filter(kv -> kv.getValue().match(pattern)).findFirst();
            while(!result.isPresent()) {
                newItem.await();
                result = tuples.entrySet().stream()
                        .filter(kv -> kv.getValue().match(pattern)).findFirst();
            }
            return result.get();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public Set<Map.Entry<Key, Tuple>> readAll() {
        return tuples.entrySet();
    }

    @Override
    public int getSize() {
        return tuples.size();
    }

    @Override
    public void delete(Key key) {
        lock.lock();
        try {
            // Remove the tuple
            tuples.remove(key);
            takeLock.remove(key);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public boolean lock(Key key, Lock requester) {
        lock.lock();
        try {
            if (!tuples.containsKey(key)) {
                return false;
            } if (!takeLock.containsKey(key)) {
                takeLock.put(key, requester);
                return true;
            } else {
                return takeLock.get(key).equals(requester);
            }
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void unlock(Lock requester) {
        lock.lock();
        try {
            Iterator<Map.Entry<Key, Lock>> i = takeLock.entrySet().iterator();
            while (i.hasNext()) {
                Map.Entry<Key, Lock> s = i.next();
                if (s.getValue().equals(requester)) {
                    Log.debug("Removing lock: {}", requester);
                    i.remove();
                }
            }
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void unlock(SocketAddress sock) {
        lock.lock();
        try {
            Iterator<Map.Entry<Key, Lock>> i = takeLock.entrySet().iterator();
            while (i.hasNext()) {
                Map.Entry<Key, Lock> s = i.next();
                if (s.getValue().getAddress().equals(sock)) {
                    Log.debug("Removing lock of server: {}", sock);
                    i.remove();
                }
            }
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void unlock(String cookies) {
        lock.lock();
        try {
            Iterator<Map.Entry<Key, Lock>> i = takeLock.entrySet().iterator();
            while (i.hasNext()) {
                Map.Entry<Key, Lock> s = i.next();
                if (s.getValue().getCookies().equals(cookies)) {
                    Log.debug("Removing lock of cookies: {}", cookies);
                    i.remove();
                }
            }
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void drop() {
        lock.lock();
        try {
            tuples.clear();
            takeLock.clear();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void printStatus() {
        lock.lock();
        try {
            System.out.printf("Total tuples: %d%n", tuples.size());
            System.out.println("Locked tuples: ");
            for (Map.Entry<Key, Tuple> item : tuples.entrySet()) {
                if (takeLock.containsKey(item.getKey())) {
                    System.out.printf("%s by %s\n", item.getValue(), takeLock.get(item.getKey()));
                }
            }
        } finally {
            lock.unlock();
        }
    }

    @Override
    public Set<Map.Entry<Key, Tuple>> takeAll(Tuple pattern, Lock requester)
            throws InterruptedException {
        lock.lock();
        try {
            Set<Map.Entry<Key, Tuple>> result = new HashSet<>();
            tuples.entrySet().stream()
                .filter(e -> e.getValue().match(pattern) && (!takeLock.containsKey(e.getKey()) || takeLock.get(e.getKey()).equals(requester)))
                .forEach(
                        result::add
                );
            return result;
        } finally {
            lock.unlock();
        }
    }
}
