package pt.tecnico.kn.rsts.benchmark;

import net.sf.jgcs.JGCSException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pt.tecnico.kn.rsts.data_type.Tuple;
import pt.tecnico.kn.rsts.network.RstsServer;
import pt.tecnico.kn.rsts.storage.MemoryStorage;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


/**
 * Benchmark class
 */
public abstract class BaseBenchmark {

    private final static Logger Log = LoggerFactory.getLogger(WriteBenchmark.class);

    protected final String GROUP_NAME = "rsts-benchmark";
    protected final int serverCount;
    protected final String serverConfig;
    protected final int clusterSize;
    protected final int operationCount;

    public BaseBenchmark(int serverCount, int clusterSize, int operationCount, String serverConfig) {
        this.serverCount = serverCount;
        this.clusterSize = clusterSize;
        this.serverConfig = serverConfig;
        this.operationCount = operationCount;
    }

    public void execute() throws JGCSException, InterruptedException {
        RstsServer[] servers = new RstsServer[serverCount];
        for(int i = 0; i < servers.length; i++) {
            servers[i] = new RstsServer(GROUP_NAME, serverConfig, new MemoryStorage());
            servers[i].start(clusterSize);
        }
        Log.info("Waiting for cluster to start up");
        Thread.sleep(5000);
        doBench(servers);
    }

    public abstract void doBench(RstsServer[] servers) throws InterruptedException;
}
