package pt.tecnico.kn.rsts.storage;

import java.io.Serializable;
import java.net.SocketAddress;
import java.util.Objects;

/**
 * The key to use in our tuple storage.
 * The key include the address of the writer and a counter
 */
public class Key implements Serializable{
    private static final long serialVersionUID = 6510826777348582474L;

    private final SocketAddress socketAddress;
    private final int counter;

    public Key(int counter, SocketAddress socketAddress) {
        this.counter = counter;
        this.socketAddress = socketAddress;
    }

    public SocketAddress getSocketAddress() {
        return socketAddress;
    }

    public int getCounter() {
        return counter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Key key = (Key) o;
        return Objects.equals(counter, key.counter) &&
                Objects.equals(socketAddress, key.socketAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(socketAddress, counter);
    }

    @Override
    public String toString() {
        return "Key{" +
                "socketAddress=" + socketAddress +
                ", counter=" + counter +
                '}';
    }
}
